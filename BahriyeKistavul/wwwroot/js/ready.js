﻿per500ms = function () {
    var error_ui = $("#blazor-error-ui");
    var reconnect_modal = $("#components-reconnect-modal").length != 0;
    if (error_ui.length > 0 && error_ui.css("display") != 'none' || reconnect_modal) {
        console.log("reloading...");
        $("#blazor-error-ui").remove();
        $("#components-reconnect-modal").remove();
        location.reload();
    } else {
        var body = $("body");

        if (body.css("opacity") == 0) {
            body.css("opacity", 1);
        }
    }
}
$(document).ready(function () {
    setInterval(per500ms, 500);

    var theme = localStorage.getItem("theme");
    if (theme != null) {
        $("html").attr("data-bs-theme", theme)
    }
});
function changeTheme() {
    var current = $("html").attr("data-bs-theme");
    var theme = current == "dark" ? "light" : "dark";
    $("html").attr("data-bs-theme", theme);
    localStorage.setItem("theme", theme);
}
function getCulture() {
    var culture = localStorage.getItem("culture");
    if (culture != null) {
        return culture;
    }
    culture = (navigator.languages && navigator.languages.length) ? navigator.languages[0] :
        navigator.userLanguage || navigator.language || navigator.browserLanguage || 'en-US';
    console.log(culture);
    return culture;
}
function setCulture(culture) {
    localStorage.setItem("culture", culture);
    console.log(culture);
}
function scrollToEl(el) {
    console.log(el);
    $("html").animate({ scrollTop: $(el).offset().top - 250 }, 100);
}

var noSleep = new NoSleep();
document.addEventListener('click', function enableNoSleep() {
    document.removeEventListener('click', enableNoSleep, false);
    noSleep.enable();
}, false);